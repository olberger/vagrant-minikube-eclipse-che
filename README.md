# Running Eclipse Che (by default in single-user mode) on a k8s running inside a Vagrant VM

This project contains the means to test Eclipse Che, deployed in
single-user mode (by default), inside a VM.

The VM runs via Vagrant, and contains a Kubernetes (k8s) mono-node
cluster. 

## Install Pre-requisites

Ensure you have vagrant installed (should also support mac/windows)

https://www.vagrantup.com/docs/installation/

### Arch
```
sudo pacman -S vagrant
```

### Ubuntu
```
sudo apt-get install vagrant
```

## Run it

Clone this repo then:

```
vagrant up
```

## Troubeshooting

Inside the VM, the k8s install is performed through `minikube`, with the `'none'` driver,
reusing the "Minikube Vagrant" project by Francesco Ciocchetti et al. hosted at
https://github.com/mintel/vagrant-minikube (this project's repo is
forked of that base project's).

Note, this installs the latest `minikube` and `kubectl` packages available for ubuntu 18.04.

Normally, Eclipse Che starts automatically upon VM startup and
provisionning, and downloads additional components (if on a slow
network, prepare for tens of minutes of delay), like the Docker
container images.

### SSH into the VM
```
vagrant ssh
```

### Check minikube is up and running

```
kubectl get -A all
```

## Accessing the Eclipe Che Web dashboard

At the end of `chectl`'s execution (the Eclipse Che installer), a link
will be displayed that should be usable to load Eclipse Che's
dashboard in the web browser running on the host machine.

This link ressembles `https://192.168.34.100.nip.io/` where
`192.168.34.100` is the Vagrant VM's "external IP". This IP should be
available to the Vagrant host, for connecting with a Web browser
running on the host.

However, as the TLS certificate which is generated during installation
will be self-signed, you will need to add the CA cert to your
browser's store, for testing Eclipse Che.

That certificate will be available in `/tmp/vagrant/cheCA.crt` on the
host, as mentioned in the installation messages.

Also note that this assumes that your DNS is able to resolve to `nip.io`
addresses. It may be worth mentioning that certain ISPs will lie to
you pretending that this domain doesn't exist... a possible solution
is installing your own DNS (`bind9` for instance, or using a public
DNS like `8.8.8.8`). YMMV

## Share stuff with the VM

We automatically mount `/tmp/vagrant` into `/home/vagrant/data`.

This is bi-directional, and achieved via [vagrant-sshfs](https://github.com/dustymabe/vagrant-sshfs), but may
not work well with all host OS (or Vagrant providers).

The Eclipse Che workspaces will created into the
`/home/vagrant/workspace-storage/` subdir, used as a `hostPath`
Persistent Volume storage. Each workspace will be contained in a
specific `workspace*` subdir.
This allows retrieving work between sessions when the VM will be
stopped (`vagrant halt`) and restarted (`vagrant resume` or `vagrant up`).

## Testing Che in multi-user mode

Set the `CHE_MULTIUSER` env variable to `true` before launching the
VM, with something like:

```
CHE_MULTIUSER=true vagrant up
```

This will result in starting Eclipse Che in multi-user mode inside the
VM, using the PostGreSQL database, and Keycloak for managing users.

In this latter case, the Persistent Volumes will be a bot different,
to save data between VM restarts.

## Other Vagrant providers

By default, the `virtualbox` Vagrant provider is used, but you may
also test other providers like `libvirt` on Linux (for qemu/kvm) or
`hyperv` on Windows.

Be cautious that this wasn't extensively tested though, so expect some
hickups.
